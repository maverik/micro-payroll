namespace �Payroll
{
    using System;
    using Cedita.Payroll.Engines;
    using Cedita.Payroll.Engines.NationalInsurance;
    using Cedita.Payroll.Engines.Paye;
    using Data;
    using Infrastructure;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IProvideTaxYearSpecifics, JsonTaxYearSpecificProvider>();

            services.AddScoped<Func<int, IPayeCalculationEngine>>(x => year =>
            {
                var engine = DefaultEngineResolver.GetEngine<IPayeCalculationEngine>(year);
                engine.SetTaxYearSpecificsProvider(x.GetRequiredService<IProvideTaxYearSpecifics>());
                engine.SetTaxYear(year);
                return engine;
            });

            services.AddScoped<Func<int, INiCalculationEngine>>(x => year =>
            {
                var engine = DefaultEngineResolver.GetEngine<INiCalculationEngine>(year);
                engine.SetTaxYearSpecificsProvider(x.GetRequiredService<IProvideTaxYearSpecifics>());
                engine.SetTaxYear(year);
                return engine;
            });

            services.AddDbContext<PayrollDbContext>(options =>
                options
                    .UseSqlServer(Configuration.GetConnectionString("Payroll")));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSession();

            services.AddHttpContextAccessor();

            services.AddScoped(x => x.GetService<IHttpContextAccessor>().HttpContext.Session);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                using (var scope = app.ApplicationServices.CreateScope())
                {
                    using (var context = scope.ServiceProvider.GetService<PayrollDbContext>())
                        context.Database.Migrate();
                }
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHttpsRedirection();
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseSession();
            app.UseMvc();
        }
    }
}
