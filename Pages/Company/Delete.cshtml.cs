﻿namespace µPayroll.Pages.Company
{
    using System.Threading.Tasks;
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Models;

    public class DeleteModel : PageModel
    {
        readonly PayrollDbContext _context;

        public DeleteModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Company Company { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Company = await _context.Companies.FindAsync(id);

            return Company == null ? (IActionResult) NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Company = await _context.Companies.FindAsync(Company.Id);

            if (Company == null) return RedirectToPage("./Index");

            _context.Companies.Remove(Company);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}