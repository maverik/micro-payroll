﻿namespace µPayroll.Pages.Company
{
    using System.Threading.Tasks;
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore.Internal;
    using Models;

    public class SetupModel : PageModel
    {
        readonly PayrollDbContext _context;

        public SetupModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Company Company { get; set; }

        public IActionResult OnGet() => _context.Companies.Any() ? (IActionResult) RedirectToPage("./Index") : Page();

        public async Task<IActionResult> OnPostAsync()
        {
            //For this sample we're going to allow only one company
            if (_context.Companies.Any()) return RedirectToPage("./Index");

            if (!ModelState.IsValid) return Page();

            _context.Companies.Add(Company);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}