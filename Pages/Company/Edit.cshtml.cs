﻿namespace µPayroll.Pages.Company
{
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class EditModel : PageModel
    {
        readonly PayrollDbContext _context;

        public EditModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Company Company { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {

            Company = await _context.Companies.Include(x => x.Address).SingleOrDefaultAsync(m => m.Id == id);

            return Company == null ? (IActionResult) NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            //Address gymnastic because we don't want to create new addresses each time
            //there's an edit. Instead we find if we already have a match for details
            var address = _context.Addresses
                .Where(x => x.Postcode == Company.Address.Postcode)
                .ToArray()
                //Use IEquality compare that won't work on SQL side
                //The check is case sensitive in implementation which is desired
                //otherwise the user will feel like we're post processing the address
                //which we are but without actually affecting their input
                .SingleOrDefault(x => x == Company.Address);

            if (address == null)
                _context.Addresses.Add(Company.Address);
            else
            {
                Company.Address = address;
                Company.AddressId = address.Id;
            }

            _context.Attach(Company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(Company.Id)) return NotFound();

                throw;
            }

            return RedirectToPage("./Index");
        }

        bool CompanyExists(int id) => _context.Companies.Any(e => e.Id == id);
    }
}