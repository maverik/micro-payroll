﻿namespace µPayroll.Pages.Company
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using Models;
    using System.Threading.Tasks;

    public class IndexModel : PageModel
    {
        readonly PayrollDbContext _context;

        public IndexModel(PayrollDbContext context) => _context = context;

        public Company Company { get; private set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Company = await _context.Companies.Include(x => x.Address).Include(x => x.Employees).SingleOrDefaultAsync();

            return Company == null
                ? RedirectToPage("./Setup")
                : (Company.Employees.Any()
                    ? Page()
                    : (IActionResult)RedirectToPage($"../Employee/Create", Company.Id));
        }
    }
}