﻿namespace µPayroll.Pages.Employee
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Threading.Tasks;

    public class DeleteModel : PageModel
    {
        readonly PayrollDbContext _context;

        public DeleteModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Employee Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Employee = await _context.Employees.SingleOrDefaultAsync(m => m.Id == id);

            return Employee == null ? (IActionResult)NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Employee = await _context.Employees.SingleOrDefaultAsync(x => x.Id == Employee.Id);

            if (Employee == null) return NotFound();

            _context.Employees.Remove(Employee);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Company/Index");
        }
    }
}