﻿namespace µPayroll.Pages.Employee
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class EditModel : PageModel
    {
        readonly PayrollDbContext _context;

        public EditModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Employee Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Employee = await _context.Employees.Include(x => x.Address).SingleOrDefaultAsync(m => m.Id == id);

            return Employee == null ? (IActionResult)NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            //Address gymnastic because we don't want to create new addresses each time
            //there's an edit. Instead we find if we already have a match for details
            var address = _context.Addresses
                .Where(x => x.Postcode == Employee.Address.Postcode)
                .ToArray()
                //Use IEquality compare that won't work on SQL side
                //The check is case sensitive in implementation which is desired
                //otherwise the user will feel like we're post processing the address
                //which we are but without actually affecting their input
                .SingleOrDefault(x => x == Employee.Address);

            if (address == null)
                _context.Addresses.Add(Employee.Address);
            else
            {
                Employee.Address = address;
                Employee.AddressId = address.Id;
            }

            _context.Attach(Employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(Employee.Id))
                    return NotFound();

                throw;
            }

            return RedirectToPage("../Company/Index");
        }

        bool EmployeeExists(int id) => _context.Employees.Any(e => e.Id == id);
    }
}