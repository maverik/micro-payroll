﻿namespace µPayroll.Pages.Employee
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Threading.Tasks;

    public class DetailsModel : PageModel
    {
        readonly PayrollDbContext _context;

        public DetailsModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Employee Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Employee = await _context.Employees.Include(x => x.Address)
                .Include(x => x.Payslips)
                .SingleOrDefaultAsync(m => m.Id == id);

            return Employee == null ? (IActionResult)NotFound() : Page();
        }
    }
}