﻿namespace µPayroll.Pages.Employee
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Threading.Tasks;

    public class CreateModel : PageModel
    {
        readonly PayrollDbContext _context;

        public CreateModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Employee Employee { get; set; } = new Employee { };

        public IActionResult OnGet(int companyId)
        {
            Employee.CompanyId = companyId;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            if (!await _context.Companies.AnyAsync(x => x.Id == Employee.CompanyId)) return NotFound();

            _context.Employees.Add(Employee);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Company/Index");
        }
    }
}