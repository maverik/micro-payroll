﻿namespace µPayroll.Pages.Payslip
{
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using System.Threading.Tasks;

    public class DeleteModel : PageModel
    {
        readonly PayrollDbContext _context;

        public DeleteModel(PayrollDbContext context) => _context = context;

        [BindProperty] public Payslip Payslip { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Payslip = await _context.Payslips.Include(x => x.Employee).SingleOrDefaultAsync(m => m.Id == id);

            return Payslip == null ? (IActionResult)NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            _context.Payslips.Remove(Payslip);

            await _context.SaveChangesAsync();

            return RedirectToPage("../Employee/Details", new { id = Payslip.EmployeeId });
        }
    }
}