﻿namespace µPayroll.Pages.Payslip
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Cedita.Payroll;
    using Cedita.Payroll.Engines.NationalInsurance;
    using Cedita.Payroll.Engines.Paye;
    using Data;
    using Infrastructure;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class CreateModel : PageModel
    {
        readonly PayrollDbContext _context;
        readonly Func<int, IPayeCalculationEngine> _getPayeeEngineForYear;
        readonly Func<int, INiCalculationEngine> _getNiEngineForYear;

        public CreateModel(PayrollDbContext context, Func<int, IPayeCalculationEngine> getPayeeEngineForYear, Func<int, INiCalculationEngine> getNiEngineForYear)
        {
            _context = context;
            _getPayeeEngineForYear = getPayeeEngineForYear;
            _getNiEngineForYear = getNiEngineForYear;
        }

        public IList<SelectListItem> NICategories { get; } = Enum.GetNames(typeof(NICategory))
            .Select(x => new SelectListItem(x, x, x == NICategory.A.ToString())).ToArray();

        public string EmployeeName { get; set; }

        [BindProperty]
        public Payslip Payslip { get; set; } = new Payslip();

        public async Task<IActionResult> OnGetAsync(int id)
        {
            var employee = await _context.Employees.SingleOrDefaultAsync(x => x.Id == id);

            if (employee == null) return NotFound();

            var lastPayslip = await _context.Payslips.Where(x => x.EmployeeId == id).OrderByDescending(x => x.Year)
                .ThenBy(x => x.Month).FirstOrDefaultAsync();

            EmployeeName = employee.Name;
            Payslip.EmployeeId = id;

            var taxPeriod = lastPayslip == null
                ? DateTime.Today
                : new DateTime(lastPayslip.Year, lastPayslip.Month, 1).AddMonths(1);

            Payslip.Month = (byte)taxPeriod.Month;
            Payslip.Year = (short)taxPeriod.Year;
            Payslip.TaxCode = lastPayslip?.TaxCode;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) return Page();

            var employee = await _context.Employees.SingleOrDefaultAsync(x => x.Id == Payslip.EmployeeId);

            if (employee == null) return NotFound();

            //Add default adjustments & payments
            Payslip.Adjustments = new List<Adjustment>
            {
                new Adjustment {Description = "SSP", AdjustmentType = AdjustmentType.StatuaryAddition},
                new Adjustment {Description = "SMP", AdjustmentType = AdjustmentType.StatuaryAddition},
                new Adjustment {Description = "OSPP", AdjustmentType = AdjustmentType.StatuaryAddition},
                new Adjustment {Description = "ASPP", AdjustmentType = AdjustmentType.StatuaryAddition},
                new Adjustment {Description = "SAP", AdjustmentType = AdjustmentType.StatuaryAddition},
                new Adjustment {Description = "Other", AdjustmentType = AdjustmentType.StatuaryAddition},

                new Adjustment {Description = "PAYE Tax", AdjustmentType = AdjustmentType.StatuaryDeduction, Value = _getPayeeEngineForYear(Payslip.Year).CalculateTaxDueForPeriod(Payslip.TaxCode, employee.AnnualSalary/12, PayPeriods.Monthly, 1)},
                new Adjustment {Description = "Employee's NI", AdjustmentType = AdjustmentType.StatuaryDeduction, Value = _getNiEngineForYear(Payslip.Year).CalculateNationalInsurance(employee.AnnualSalary/12, char.Parse(Payslip.NICategory.ToString()), PayPeriods.Monthly).EmployeeNi },
                new Adjustment {Description = "SLR", AdjustmentType = AdjustmentType.StatuaryDeduction},
                new Adjustment {Description = "AOE", AdjustmentType = AdjustmentType.StatuaryDeduction},
                new Adjustment {Description = "Pension", AdjustmentType = AdjustmentType.StatuaryDeduction},
                new Adjustment {Description = "Other", AdjustmentType = AdjustmentType.StatuaryDeduction}
            };

            Payslip.Payments = new List<QuantitativeAdjustment> { new QuantitativeAdjustment { Description = "Payment", UnitValue = employee.AnnualSalary / 12, Quantity = 1 } };

            _context.Payslips.Add(Payslip);

            await _context.SaveChangesAsync();

            return RedirectToPage("Details", new { id = Payslip.Id });
        }
    }
}