﻿namespace µPayroll.Pages.Payslip
{
    using System.Threading.Tasks;
    using Data;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class DetailsModel : PageModel
    {
        readonly PayrollDbContext _context;

        public DetailsModel(PayrollDbContext context) => _context = context;

        public string EmployeeName { get; set; }

        public bool IsReadOnly => Payslip.IssuedAt.HasValue;

        public Payslip Payslip { get; set; }

        [BindProperty]
        public QuantitativeAdjustment CurrentPayment { get; set; }

        [BindProperty]
        public Adjustment CurrentAdjustment { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Payslip = await _context.Payslips
                .Include(x => x.Adjustments)
                .Include(x => x.Payments)
                .Include(x => x.Employee)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (Payslip == null) return NotFound();

            EmployeeName = Payslip.Employee.Name;

            return Page();
        }


        public async Task<IActionResult> OnGetNewPayment(int id)
        {
            if (!await _context.Payslips.AnyAsync(x => x.Id == id)) return NotFound();

            _context.QuantitativeAdjustments.Add(new QuantitativeAdjustment { PayslipPaymentId = id });
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id });
        }

        public async Task<IActionResult> OnPostDiscardPaymentAsync()
        {
            if (!await _context.QuantitativeAdjustments.AnyAsync(x => x.Id == CurrentPayment.Id)) return NotFound();

            _context.QuantitativeAdjustments.Remove(CurrentPayment);
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id = CurrentPayment.PayslipPaymentId });
        }

        public async Task<IActionResult> OnPostUpdatePaymentAsync()
        {
            if (!await _context.QuantitativeAdjustments.AnyAsync(x => x.Id == CurrentPayment.Id)) return NotFound();

            _context.QuantitativeAdjustments.Update(CurrentPayment);
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id = CurrentPayment.PayslipPaymentId });
        }

        public async Task<IActionResult> OnGetNewAdjustment(int id)
        {
            if (!await _context.Payslips.AnyAsync(x => x.Id == id)) return NotFound();

            _context.Adjustments.Add(new Adjustment { PayslipAdjustmentId = id });
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id });
        }

        public async Task<IActionResult> OnPostDiscardAdjustmentAsync()
        {
            if (!await _context.Adjustments.AnyAsync(x => x.Id == CurrentAdjustment.Id)) return NotFound();

            _context.Adjustments.Remove(CurrentAdjustment);
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id = CurrentAdjustment.PayslipAdjustmentId });
        }

        public async Task<IActionResult> OnPostUpdateAdjustmentAsync()
        {
            if (!await _context.Adjustments.AnyAsync(x => x.Id == CurrentAdjustment.Id)) return NotFound();

            _context.Adjustments.Update(CurrentAdjustment);
            await _context.SaveChangesAsync();

            return RedirectToPage(new { id = CurrentAdjustment.PayslipAdjustmentId });
        }

    }
}