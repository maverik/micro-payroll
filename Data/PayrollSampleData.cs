﻿namespace µPayroll.Data
{
    using Models;
    using µPayroll.Infrastructure;

    public static class PayrollSampleData
    {
        public static Address[] Addresses { get; } =
        {
            new Address
            {
                Id = 1,
                Line1 = "23 Infested Carrion street",
                City = "Straesea",
                Postcode = "ST1 21L"
            },

            new Address
            {
                Id = 2,
                Line1 = "912 Sunder lands",
                City = "Straesea",
                Postcode = "ST11 9K2"
            },

            new Address
            {
                Id = 3,
                Line1 = "812 Blood-bank road",
                City = "Straesea",
                Postcode = "ST7 I92"
            }
        };

        public static Company[] Companies { get; } =
        {
            new Company
            {
                Id = 1,
                Name = "Heavens On Earth",
                AddressId = 1
            }
        };

        public static Employee[] Employees { get; } =
        {
            new Employee
            {
                Id = 1,
                Name = "Eric Loof",
                AddressId = 2,
                CompanyId = 1,
                AnnualSalary = 48000,
                NICode = "NI231317"            },

            new Employee
            {
                Id = 2,
                Name = "Paula Tucker",
                AddressId = 2,
                CompanyId = 1,
                AnnualSalary = 24000,
                NICode = "NI918271",
                ManagerId = 1,
            },

            new Employee
            {
                Id = 3,
                Name = "Brian Kinney",
                AddressId = 3,
                CompanyId = 1,
                AnnualSalary = 36000,
                NICode = "NI007008",
                ManagerId = 1,
            }
        };
    }
}