﻿namespace µPayroll.Data
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using µPayroll.Infrastructure;

    public sealed class PayrollDbContext : DbContext
    {
        // ReSharper disable once SuggestBaseTypeForParameter
        public PayrollDbContext(DbContextOptions<PayrollDbContext> options) : base(options) { }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Payslip> Payslips { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<QuantitativeAdjustment> QuantitativeAdjustments { get; set; }

        public DbSet<Adjustment> Adjustments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>(e =>
            {
                e.HasKey(x => x.Id);
                e.HasIndex(x => x.Postcode);

                e.Property(x => x.Line2).IsRequired(false);
                e.Property(x => x.Postcode).IsUnicode(false);

                e.HasData(PayrollSampleData.Addresses);
            });

            modelBuilder.Entity<Company>(e =>
            {
                e.HasKey(x => x.Id);
                e.HasIndex(x => x.Name);

                //while this scenario isn't going to use this but going by typical configuration
                e.HasOne(x => x.Address)
                    .WithMany(x => x.Companies)
                    .HasForeignKey(x => x.AddressId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Restrict);

                e.HasData(PayrollSampleData.Companies);
            });

            modelBuilder.Entity<Employee>(e =>
            {
                e.HasKey(x => x.Id);
                e.HasIndex(x => x.Name);

                e.HasOne(x => x.Company)
                    .WithMany(x => x.Employees)
                    .HasForeignKey(x => x.CompanyId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                e.HasOne(x => x.Address)
                    .WithMany(x => x.Employees)
                    .HasForeignKey(x => x.AddressId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Restrict);

                e.HasMany(x => x.Payslips)
                    .WithOne(x => x.Employee)
                    .HasForeignKey(x => x.EmployeeId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                e.HasOne(x => x.Manager)
                    .WithMany(x => x.ManagedEmployees)
                    .HasForeignKey(x => x.ManagerId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                // while the calculations may need decimal, the final result is expected to fall in decimal(34,4) range
                // I'm not sure how many employees are going to have their salary in half shilling(?) exactly
                e.Property(x => x.AnnualSalary).HasColumnType("money");

                e.HasData(PayrollSampleData.Employees);
            });

            modelBuilder.Entity<Payslip>(e =>
            {
                e.HasKey(x => x.Id);

                //An employee can only have one payslip in a year/month combo
                e.HasIndex(x => new { x.EmployeeId, x.Year, x.Month }).IsUnique();

                //I'm assuming these aren't *required*.
                //The employee may very well need to pay company!
                e.HasMany(x => x.Payments)
                    .WithOne(x => x.PayslipPayment)
                    .HasForeignKey(x => x.PayslipPaymentId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                e.HasMany(x => x.Adjustments)
                    .WithOne(x => x.PayslipAdjustment)
                    .HasForeignKey(x => x.PayslipAdjustmentId)
                    .HasPrincipalKey(x => x.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                e.Property(x => x.DbIssuedAt);

                e.Property(x => x.NICategory)
                    .HasConversion(x => x.ToString(), x => Enum.Parse<NICategory>(x, true));

                e.Ignore(x => x.IssuedAt);
            });

            modelBuilder.Entity<Adjustment>(e =>
            {
                e.HasKey(x => x.Id);
                e.Property(x => x.Value).HasColumnType("money");
            });

            modelBuilder.Entity<QuantitativeAdjustment>(e =>
            {
                e.HasKey(x => x.Id);
                e.Property(x => x.Value).HasColumnType("money");
                //not strictly money but since the type is set to decimal for easier
                //operations avoiding casts
                e.Property(x => x.Quantity).HasColumnType("money");
                e.Property(x => x.UnitValue).HasColumnType("money");
                e.Ignore(x => x.Value);
            });
        }
    }
}