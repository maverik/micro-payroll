﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class RemoveStatuaryCodesFromQuantitativeAdjustments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdjustmentType",
                table: "QuantitativeAdjustment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "AdjustmentType",
                table: "QuantitativeAdjustment",
                nullable: false,
                defaultValue: (byte)0);
        }
    }
}
