﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class MoveContractReferenceToEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractReference",
                table: "Payslips");

            migrationBuilder.AddColumn<string>(
                name: "ContractReference",
                table: "Employees",
                maxLength: 60,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractReference",
                table: "Employees");

            migrationBuilder.AddColumn<string>(
                name: "ContractReference",
                table: "Payslips",
                maxLength: 60,
                nullable: true);
        }
    }
}
