﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class MakeIssueDateOptionalOnPayslip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "DbIssuedAt",
                table: "Payslips",
                nullable: true,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "DbIssuedAt",
                table: "Payslips",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
