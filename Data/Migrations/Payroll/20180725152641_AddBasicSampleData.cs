﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class AddBasicSampleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "Country", "Line1", "Line2", "Postcode" },
                values: new object[] { 1, "Straesea", "United Kingdom", "23 Infested Carrion street", null, "ST1 21L" });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "Country", "Line1", "Line2", "Postcode" },
                values: new object[] { 2, "Straesea", "United Kingdom", "912 Sunder lands", null, "ST11 9K2" });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "Country", "Line1", "Line2", "Postcode" },
                values: new object[] { 3, "Straesea", "United Kingdom", "812 Blood-bank road", null, "ST7 I92" });

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "AddressId", "Name" },
                values: new object[] { 1, 1, "Heavens On Earth" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AddressId", "AnnualSalary", "CompanyId", "EmployeeId", "ManagerId", "NICode", "Name" },
                values: new object[] { 1, 2, 48000m, 1, null, null, "NI231317", "Eric Loof" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AddressId", "AnnualSalary", "CompanyId", "EmployeeId", "ManagerId", "NICode", "Name" },
                values: new object[] { 2, 2, 24000m, 1, null, 1, "NI918271", "Paula Tucker" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AddressId", "AnnualSalary", "CompanyId", "EmployeeId", "ManagerId", "NICode", "Name" },
                values: new object[] { 3, 3, 36000m, 1, null, 1, "NI007008", "Brian Kinney" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
