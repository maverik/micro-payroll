﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class ExposeAdjustments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Adjustment_Payslips_PayslipAdjustmentId",
                table: "Adjustment");

            migrationBuilder.DropForeignKey(
                name: "FK_QuantitativeAdjustment_Payslips_PayslipPaymentId",
                table: "QuantitativeAdjustment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuantitativeAdjustment",
                table: "QuantitativeAdjustment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Adjustment",
                table: "Adjustment");

            migrationBuilder.RenameTable(
                name: "QuantitativeAdjustment",
                newName: "QuantitativeAdjustments");

            migrationBuilder.RenameTable(
                name: "Adjustment",
                newName: "Adjustments");

            migrationBuilder.RenameIndex(
                name: "IX_QuantitativeAdjustment_PayslipPaymentId",
                table: "QuantitativeAdjustments",
                newName: "IX_QuantitativeAdjustments_PayslipPaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_Adjustment_PayslipAdjustmentId",
                table: "Adjustments",
                newName: "IX_Adjustments_PayslipAdjustmentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuantitativeAdjustments",
                table: "QuantitativeAdjustments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Adjustments",
                table: "Adjustments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Adjustments_Payslips_PayslipAdjustmentId",
                table: "Adjustments",
                column: "PayslipAdjustmentId",
                principalTable: "Payslips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuantitativeAdjustments_Payslips_PayslipPaymentId",
                table: "QuantitativeAdjustments",
                column: "PayslipPaymentId",
                principalTable: "Payslips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Adjustments_Payslips_PayslipAdjustmentId",
                table: "Adjustments");

            migrationBuilder.DropForeignKey(
                name: "FK_QuantitativeAdjustments_Payslips_PayslipPaymentId",
                table: "QuantitativeAdjustments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuantitativeAdjustments",
                table: "QuantitativeAdjustments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Adjustments",
                table: "Adjustments");

            migrationBuilder.RenameTable(
                name: "QuantitativeAdjustments",
                newName: "QuantitativeAdjustment");

            migrationBuilder.RenameTable(
                name: "Adjustments",
                newName: "Adjustment");

            migrationBuilder.RenameIndex(
                name: "IX_QuantitativeAdjustments_PayslipPaymentId",
                table: "QuantitativeAdjustment",
                newName: "IX_QuantitativeAdjustment_PayslipPaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_Adjustments_PayslipAdjustmentId",
                table: "Adjustment",
                newName: "IX_Adjustment_PayslipAdjustmentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuantitativeAdjustment",
                table: "QuantitativeAdjustment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Adjustment",
                table: "Adjustment",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Adjustment_Payslips_PayslipAdjustmentId",
                table: "Adjustment",
                column: "PayslipAdjustmentId",
                principalTable: "Payslips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuantitativeAdjustment_Payslips_PayslipPaymentId",
                table: "QuantitativeAdjustment",
                column: "PayslipPaymentId",
                principalTable: "Payslips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
