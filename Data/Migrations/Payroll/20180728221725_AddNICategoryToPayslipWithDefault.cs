﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace µPayroll.Data.Migrations.Payroll
{
    public partial class AddNICategoryToPayslipWithDefault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NICategory",
                table: "Payslips",
                maxLength: 1,
                nullable: false,
                defaultValue: "A");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NICategory",
                table: "Payslips");
        }
    }
}
