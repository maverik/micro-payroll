﻿namespace µPayroll.Infrastructure
{
    using System.ComponentModel.DataAnnotations;

    public interface IAdjustment
    {
        [StringLength(100)]
        string Description { get; }

        [Required]
        decimal Value { get; }
    }
}