﻿namespace µPayroll.Infrastructure
{
    public enum AdjustmentType : byte
    {
        Unknown,
        StatuaryAddition,
        StatuaryDeduction,
    }
}