﻿namespace µPayroll.Data.Infrastructure
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    //Source imported from https://www.countrycode.org/
    public class Countries : Dictionary<Countries.IsoCountryCode, string>
    {
        public static Countries Default { get; } = new Countries();

        [SuppressMessage("ReSharper", "InconsistentNaming", Justification =
            "ISO Country codes are two character abbreviations")]
        public enum IsoCountryCode : byte
        {
            AF,
            AL,
            DZ,
            AS,
            AD,
            AO,
            AI,
            AQ,
            AG,
            AR,
            AM,
            AW,
            AU,
            AT,
            AZ,
            BS,
            BH,
            BD,
            BB,
            BY,
            BE,
            BZ,
            BJ,
            BM,
            BT,
            BO,
            BA,
            BW,
            BR,
            IO,
            VG,
            BN,
            BG,
            BF,
            BI,
            KH,
            CM,
            CA,
            CV,
            KY,
            CF,
            TD,
            CL,
            CN,
            CX,
            CC,
            CO,
            KM,
            CK,
            CR,
            HR,
            CU,
            CW,
            CY,
            CZ,
            CD,
            DK,
            DJ,
            DM,
            DO,
            TL,
            EC,
            EG,
            SV,
            GQ,
            ER,
            EE,
            ET,
            FK,
            FO,
            FJ,
            FI,
            FR,
            PF,
            GA,
            GM,
            GE,
            DE,
            GH,
            GI,
            GR,
            GL,
            GD,
            GU,
            GT,
            GG,
            GN,
            GW,
            GY,
            HT,
            HN,
            HK,
            HU,
            IS,
            IN,
            ID,
            IR,
            IQ,
            IE,
            IM,
            IL,
            IT,
            CI,
            JM,
            JP,
            JE,
            JO,
            KZ,
            KE,
            KI,
            XK,
            KW,
            KG,
            LA,
            LV,
            LB,
            LS,
            LR,
            LY,
            LI,
            LT,
            LU,
            MO,
            MK,
            MG,
            MW,
            MY,
            MV,
            ML,
            MT,
            MH,
            MR,
            MU,
            YT,
            MX,
            FM,
            MD,
            MC,
            MN,
            ME,
            MS,
            MA,
            MZ,
            MM,
            NA,
            NR,
            NP,
            NL,
            AN,
            NC,
            NZ,
            NI,
            NE,
            NG,
            NU,
            KP,
            MP,
            NO,
            OM,
            PK,
            PW,
            PS,
            PA,
            PG,
            PY,
            PE,
            PH,
            PN,
            PL,
            PT,
            PR,
            QA,
            CG,
            RE,
            RO,
            RU,
            RW,
            BL,
            SH,
            KN,
            LC,
            MF,
            PM,
            VC,
            WS,
            SM,
            ST,
            SA,
            SN,
            RS,
            SC,
            SL,
            SG,
            SX,
            SK,
            SI,
            SB,
            SO,
            ZA,
            KR,
            SS,
            ES,
            LK,
            SD,
            SR,
            SJ,
            SZ,
            SE,
            CH,
            SY,
            TW,
            TJ,
            TZ,
            TH,
            TG,
            TK,
            TO,
            TT,
            TN,
            TR,
            TM,
            TC,
            TV,
            VI,
            UG,
            UA,
            AE,
            GB,
            US,
            UY,
            UZ,
            VU,
            VA,
            VE,
            VN,
            WF,
            EH,
            YE,
            ZM,
            ZW
        }

        Countries()
        {
            Add(IsoCountryCode.AF, "Afghanistan");
            Add(IsoCountryCode.AL, "Albania");
            Add(IsoCountryCode.DZ, "Algeria");
            Add(IsoCountryCode.AS, "American Samoa");
            Add(IsoCountryCode.AD, "Andorra");
            Add(IsoCountryCode.AO, "Angola");
            Add(IsoCountryCode.AI, "Anguilla");
            Add(IsoCountryCode.AQ, "Antarctica");
            Add(IsoCountryCode.AG, "Antigua and Barbuda");
            Add(IsoCountryCode.AR, "Argentina");
            Add(IsoCountryCode.AM, "Armenia");
            Add(IsoCountryCode.AW, "Aruba");
            Add(IsoCountryCode.AU, "Australia");
            Add(IsoCountryCode.AT, "Austria");
            Add(IsoCountryCode.AZ, "Azerbaijan");
            Add(IsoCountryCode.BS, "Bahamas");
            Add(IsoCountryCode.BH, "Bahrain");
            Add(IsoCountryCode.BD, "Bangladesh");
            Add(IsoCountryCode.BB, "Barbados");
            Add(IsoCountryCode.BY, "Belarus");
            Add(IsoCountryCode.BE, "Belgium");
            Add(IsoCountryCode.BZ, "Belize");
            Add(IsoCountryCode.BJ, "Benin");
            Add(IsoCountryCode.BM, "Bermuda");
            Add(IsoCountryCode.BT, "Bhutan");
            Add(IsoCountryCode.BO, "Bolivia");
            Add(IsoCountryCode.BA, "Bosnia and Herzegovina");
            Add(IsoCountryCode.BW, "Botswana");
            Add(IsoCountryCode.BR, "Brazil");
            Add(IsoCountryCode.IO, "British Indian Ocean Territory");
            Add(IsoCountryCode.VG, "British Virgin Islands");
            Add(IsoCountryCode.BN, "Brunei");
            Add(IsoCountryCode.BG, "Bulgaria");
            Add(IsoCountryCode.BF, "Burkina Faso");
            Add(IsoCountryCode.BI, "Burundi");
            Add(IsoCountryCode.KH, "Cambodia");
            Add(IsoCountryCode.CM, "Cameroon");
            Add(IsoCountryCode.CA, "Canada");
            Add(IsoCountryCode.CV, "Cape Verde");
            Add(IsoCountryCode.KY, "Cayman Islands");
            Add(IsoCountryCode.CF, "Central African Republic");
            Add(IsoCountryCode.TD, "Chad");
            Add(IsoCountryCode.CL, "Chile");
            Add(IsoCountryCode.CN, "China");
            Add(IsoCountryCode.CX, "Christmas Island");
            Add(IsoCountryCode.CC, "Cocos Islands");
            Add(IsoCountryCode.CO, "Colombia");
            Add(IsoCountryCode.KM, "Comoros");
            Add(IsoCountryCode.CK, "Cook Islands");
            Add(IsoCountryCode.CR, "Costa Rica");
            Add(IsoCountryCode.HR, "Croatia");
            Add(IsoCountryCode.CU, "Cuba");
            Add(IsoCountryCode.CW, "Curacao");
            Add(IsoCountryCode.CY, "Cyprus");
            Add(IsoCountryCode.CZ, "Czech Republic");
            Add(IsoCountryCode.CD, "Democratic Republic of the Congo");
            Add(IsoCountryCode.DK, "Denmark");
            Add(IsoCountryCode.DJ, "Djibouti");
            Add(IsoCountryCode.DM, "Dominica");
            Add(IsoCountryCode.DO, "Dominican Republic");
            Add(IsoCountryCode.TL, "East Timor");
            Add(IsoCountryCode.EC, "Ecuador");
            Add(IsoCountryCode.EG, "Egypt");
            Add(IsoCountryCode.SV, "El Salvador");
            Add(IsoCountryCode.GQ, "Equatorial Guinea");
            Add(IsoCountryCode.ER, "Eritrea");
            Add(IsoCountryCode.EE, "Estonia");
            Add(IsoCountryCode.ET, "Ethiopia");
            Add(IsoCountryCode.FK, "Falkland Islands");
            Add(IsoCountryCode.FO, "Faroe Islands");
            Add(IsoCountryCode.FJ, "Fiji");
            Add(IsoCountryCode.FI, "Finland");
            Add(IsoCountryCode.FR, "France");
            Add(IsoCountryCode.PF, "French Polynesia");
            Add(IsoCountryCode.GA, "Gabon");
            Add(IsoCountryCode.GM, "Gambia");
            Add(IsoCountryCode.GE, "Georgia");
            Add(IsoCountryCode.DE, "Germany");
            Add(IsoCountryCode.GH, "Ghana");
            Add(IsoCountryCode.GI, "Gibraltar");
            Add(IsoCountryCode.GR, "Greece");
            Add(IsoCountryCode.GL, "Greenland");
            Add(IsoCountryCode.GD, "Grenada");
            Add(IsoCountryCode.GU, "Guam");
            Add(IsoCountryCode.GT, "Guatemala");
            Add(IsoCountryCode.GG, "Guernsey");
            Add(IsoCountryCode.GN, "Guinea");
            Add(IsoCountryCode.GW, "Guinea-Bissau");
            Add(IsoCountryCode.GY, "Guyana");
            Add(IsoCountryCode.HT, "Haiti");
            Add(IsoCountryCode.HN, "Honduras");
            Add(IsoCountryCode.HK, "Hong Kong");
            Add(IsoCountryCode.HU, "Hungary");
            Add(IsoCountryCode.IS, "Iceland");
            Add(IsoCountryCode.IN, "India");
            Add(IsoCountryCode.ID, "Indonesia");
            Add(IsoCountryCode.IR, "Iran");
            Add(IsoCountryCode.IQ, "Iraq");
            Add(IsoCountryCode.IE, "Ireland");
            Add(IsoCountryCode.IM, "Isle of Man");
            Add(IsoCountryCode.IL, "Israel");
            Add(IsoCountryCode.IT, "Italy");
            Add(IsoCountryCode.CI, "Ivory Coast");
            Add(IsoCountryCode.JM, "Jamaica");
            Add(IsoCountryCode.JP, "Japan");
            Add(IsoCountryCode.JE, "Jersey");
            Add(IsoCountryCode.JO, "Jordan");
            Add(IsoCountryCode.KZ, "Kazakhstan");
            Add(IsoCountryCode.KE, "Kenya");
            Add(IsoCountryCode.KI, "Kiribati");
            Add(IsoCountryCode.XK, "Kosovo");
            Add(IsoCountryCode.KW, "Kuwait");
            Add(IsoCountryCode.KG, "Kyrgyzstan");
            Add(IsoCountryCode.LA, "Laos");
            Add(IsoCountryCode.LV, "Latvia");
            Add(IsoCountryCode.LB, "Lebanon");
            Add(IsoCountryCode.LS, "Lesotho");
            Add(IsoCountryCode.LR, "Liberia");
            Add(IsoCountryCode.LY, "Libya");
            Add(IsoCountryCode.LI, "Liechtenstein");
            Add(IsoCountryCode.LT, "Lithuania");
            Add(IsoCountryCode.LU, "Luxembourg");
            Add(IsoCountryCode.MO, "Macau");
            Add(IsoCountryCode.MK, "Macedonia");
            Add(IsoCountryCode.MG, "Madagascar");
            Add(IsoCountryCode.MW, "Malawi");
            Add(IsoCountryCode.MY, "Malaysia");
            Add(IsoCountryCode.MV, "Maldives");
            Add(IsoCountryCode.ML, "Mali");
            Add(IsoCountryCode.MT, "Malta");
            Add(IsoCountryCode.MH, "Marshall Islands");
            Add(IsoCountryCode.MR, "Mauritania");
            Add(IsoCountryCode.MU, "Mauritius");
            Add(IsoCountryCode.YT, "Mayotte");
            Add(IsoCountryCode.MX, "Mexico");
            Add(IsoCountryCode.FM, "Micronesia");
            Add(IsoCountryCode.MD, "Moldova");
            Add(IsoCountryCode.MC, "Monaco");
            Add(IsoCountryCode.MN, "Mongolia");
            Add(IsoCountryCode.ME, "Montenegro");
            Add(IsoCountryCode.MS, "Montserrat");
            Add(IsoCountryCode.MA, "Morocco");
            Add(IsoCountryCode.MZ, "Mozambique");
            Add(IsoCountryCode.MM, "Myanmar");
            Add(IsoCountryCode.NA, "Namibia");
            Add(IsoCountryCode.NR, "Nauru");
            Add(IsoCountryCode.NP, "Nepal");
            Add(IsoCountryCode.NL, "Netherlands");
            Add(IsoCountryCode.AN, "Netherlands Antilles");
            Add(IsoCountryCode.NC, "New Caledonia");
            Add(IsoCountryCode.NZ, "New Zealand");
            Add(IsoCountryCode.NI, "Nicaragua");
            Add(IsoCountryCode.NE, "Niger");
            Add(IsoCountryCode.NG, "Nigeria");
            Add(IsoCountryCode.NU, "Niue");
            Add(IsoCountryCode.KP, "North Korea");
            Add(IsoCountryCode.MP, "Northern Mariana Islands");
            Add(IsoCountryCode.NO, "Norway");
            Add(IsoCountryCode.OM, "Oman");
            Add(IsoCountryCode.PK, "Pakistan");
            Add(IsoCountryCode.PW, "Palau");
            Add(IsoCountryCode.PS, "Palestine");
            Add(IsoCountryCode.PA, "Panama");
            Add(IsoCountryCode.PG, "Papua New Guinea");
            Add(IsoCountryCode.PY, "Paraguay");
            Add(IsoCountryCode.PE, "Peru");
            Add(IsoCountryCode.PH, "Philippines");
            Add(IsoCountryCode.PN, "Pitcairn");
            Add(IsoCountryCode.PL, "Poland");
            Add(IsoCountryCode.PT, "Portugal");
            Add(IsoCountryCode.PR, "Puerto Rico");
            Add(IsoCountryCode.QA, "Qatar");
            Add(IsoCountryCode.CG, "Republic of the Congo");
            Add(IsoCountryCode.RE, "Reunion");
            Add(IsoCountryCode.RO, "Romania");
            Add(IsoCountryCode.RU, "Russia");
            Add(IsoCountryCode.RW, "Rwanda");
            Add(IsoCountryCode.BL, "Saint Barthelemy");
            Add(IsoCountryCode.SH, "Saint Helena");
            Add(IsoCountryCode.KN, "Saint Kitts and Nevis");
            Add(IsoCountryCode.LC, "Saint Lucia");
            Add(IsoCountryCode.MF, "Saint Martin");
            Add(IsoCountryCode.PM, "Saint Pierre and Miquelon");
            Add(IsoCountryCode.VC, "Saint Vincent and the Grenadines");
            Add(IsoCountryCode.WS, "Samoa");
            Add(IsoCountryCode.SM, "San Marino");
            Add(IsoCountryCode.ST, "Sao Tome and Principe");
            Add(IsoCountryCode.SA, "Saudi Arabia");
            Add(IsoCountryCode.SN, "Senegal");
            Add(IsoCountryCode.RS, "Serbia");
            Add(IsoCountryCode.SC, "Seychelles");
            Add(IsoCountryCode.SL, "Sierra Leone");
            Add(IsoCountryCode.SG, "Singapore");
            Add(IsoCountryCode.SX, "Sint Maarten");
            Add(IsoCountryCode.SK, "Slovakia");
            Add(IsoCountryCode.SI, "Slovenia");
            Add(IsoCountryCode.SB, "Solomon Islands");
            Add(IsoCountryCode.SO, "Somalia");
            Add(IsoCountryCode.ZA, "South Africa");
            Add(IsoCountryCode.KR, "South Korea");
            Add(IsoCountryCode.SS, "South Sudan");
            Add(IsoCountryCode.ES, "Spain");
            Add(IsoCountryCode.LK, "Sri Lanka");
            Add(IsoCountryCode.SD, "Sudan");
            Add(IsoCountryCode.SR, "Suriname");
            Add(IsoCountryCode.SJ, "Svalbard and Jan Mayen");
            Add(IsoCountryCode.SZ, "Swaziland");
            Add(IsoCountryCode.SE, "Sweden");
            Add(IsoCountryCode.CH, "Switzerland");
            Add(IsoCountryCode.SY, "Syria");
            Add(IsoCountryCode.TW, "Taiwan");
            Add(IsoCountryCode.TJ, "Tajikistan");
            Add(IsoCountryCode.TZ, "Tanzania");
            Add(IsoCountryCode.TH, "Thailand");
            Add(IsoCountryCode.TG, "Togo");
            Add(IsoCountryCode.TK, "Tokelau");
            Add(IsoCountryCode.TO, "Tonga");
            Add(IsoCountryCode.TT, "Trinidad and Tobago");
            Add(IsoCountryCode.TN, "Tunisia");
            Add(IsoCountryCode.TR, "Turkey");
            Add(IsoCountryCode.TM, "Turkmenistan");
            Add(IsoCountryCode.TC, "Turks and Caicos Islands");
            Add(IsoCountryCode.TV, "Tuvalu");
            Add(IsoCountryCode.VI, "U.S. Virgin Islands");
            Add(IsoCountryCode.UG, "Uganda");
            Add(IsoCountryCode.UA, "Ukraine");
            Add(IsoCountryCode.AE, "United Arab Emirates");
            Add(IsoCountryCode.GB, "United Kingdom");
            Add(IsoCountryCode.US, "United States");
            Add(IsoCountryCode.UY, "Uruguay");
            Add(IsoCountryCode.UZ, "Uzbekistan");
            Add(IsoCountryCode.VU, "Vanuatu");
            Add(IsoCountryCode.VA, "Vatican");
            Add(IsoCountryCode.VE, "Venezuela");
            Add(IsoCountryCode.VN, "Vietnam");
            Add(IsoCountryCode.WF, "Wallis and Futuna");
            Add(IsoCountryCode.EH, "Western Sahara");
            Add(IsoCountryCode.YE, "Yemen");
            Add(IsoCountryCode.ZM, "Zambia");
            Add(IsoCountryCode.ZW, "Zimbabwe");
        }
    }
}