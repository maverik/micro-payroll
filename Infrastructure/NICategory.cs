﻿namespace µPayroll.Infrastructure
{
    /// <summary>
    /// NI categories as defined at https://www.gov.uk/national-insurance-rates-letters/category-letters
    /// </summary>
    public enum NICategory : byte
    {
        /// <summary>
        /// All employees apart from those in groups B, C, J, H, M and Z in this table
        /// </summary>
        A,
        /// <summary>
        /// Married women and widows entitled to pay reduced National Insurance
        /// </summary>
        B,
        /// <summary>
        /// Employees over the State Pension age
        /// </summary>
        C,
        /// <summary>
        /// Employees who can defer National Insurance because they’re already paying it in another job
        /// </summary>
        H,
        /// <summary>
        /// Apprentice under 25
        /// </summary>
        J,
        /// <summary>
        /// Employees under 21
        /// </summary>
        M,
        /// <summary>
        /// Employees under 21 who can defer National Insurance because they’re already paying it in another job
        /// </summary>
        Z
    }
}
