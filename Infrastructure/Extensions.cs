﻿namespace µPayroll.Infrastructure
{
    using System.Text;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;

    public static class Extensions
    {
        public static string CreateKey<T>(this ISession session, string subKey) => $"{CreateKey<T>(session)}-{subKey}";

        public static string CreateKey<T>(this ISession session) => CreateKey(session, typeof(T).MetadataToken.ToString());

        public static string CreateKey(this ISession session, string key) => $"{session.Id}-{key}";

        /// <summary>
        /// Convenience overload when there's only one item of a type
        /// </summary>
        public static T Get<T>(this ISession session) => Get<T>(session, CreateKey<T>(session));

        /// <summary>
        /// Get concrete item from session stored as json string utf8 byte array
        /// </summary>
        public static T Get<T>(this ISession session, string key)
        {
            if (!session.TryGetValue(key, out var rawValue)) return default(T);

            var value = Encoding.UTF8.GetString(rawValue);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// Convenience overload when there's only one item of a type
        /// </summary>
        public static T Pop<T>(this ISession session) => Pop<T>(session, CreateKey<T>(session));

        /// <summary>
        /// Get concrete item from session stored as json string utf8 byte array & remove it from session
        /// </summary>
        public static T Pop<T>(this ISession session, string key)
        {
            var value = Get<T>(session, key);

            session.Remove(key);

            return value;
        }

        /// <summary>
        /// Convenience overload when there's only one item of a type
        /// </summary>
        public static void Set<T>(this ISession session, T value) => Set(session, CreateKey<T>(session), value);

        /// <summary>
        /// Set concrete item in session stored as json string utf8 byte array
        /// </summary>
        public static void Set<T>(this ISession session, string key, T value)
        {
            var jsonValue = JsonConvert.SerializeObject(value);
            var rawValue = Encoding.UTF8.GetBytes(jsonValue);

            session.Set(key, rawValue);
        }
    }
}
