# Micro-Payroll / Employee Management App

## Employee Management
### Features
- Support multiple employees
- Allow Creation / Update of employees
- Basic entry of details
- Review payslips

#### Company Details
- Name
- Address

#### Employee Structure
- Full Name
- Address
- Tax Code (e.g. [1185L](https://www.gov.uk/tax-codes))
- NI Code
- Annual Salary

## Payroll
### Features
- Calculate Payroll net values for given month
    - Salary 12Kpa -> Month 1 yields Net payment from 1K Gross payment
- Use [Cedita.Payroll](https://github.com/cedita/Cedita.Payroll/tree/rudiv/v2-di)
- PDF payslip

### Raw spec

We would like to see a system that contains 2 core elements to it, Employee Management and Payroll. The system should support multiple employees, allowing entry of their basic details required for payroll (outlined below), an entry of their salaries and the ability to review payslips. The system would belong to a single company (the employer), and so basic company details should be available for this company (Name, Address).

Employees have an Address, Tax Code (e.g. 1185L - https://www.gov.uk/tax-codes), NI Code, Yearly Salary

There should be an interface to Create and Update employees in the system.

Payroll should have an interface to calculate payroll net values for a given month. For example, on a Salary of £12,000 annually, calculating Month 1 would calculate the Net Payment from a £1,000 Gross payment.

Payroll should be implemented using Cedita.Payroll, a DI Friendly version is available here: https://github.com/cedita/Cedita.Payroll/tree/rudiv/v2-di

Ideally, a PDF would be generated as a payslip using any method you desire for generation, a sample payslip is attached. As before content of the payslip is not what we will be assessing, merely method of implementation and retrieval against an employee.