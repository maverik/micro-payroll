﻿namespace µPayroll.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    public class Payslip
    {
        public int Id { get; set; }

        /// <summary>
        /// Used to store datetime as tz agnostic timestamp
        /// </summary>
        internal long? DbIssuedAt
        {
            get => IssuedAt?.ToFileTimeUtc();
            set => IssuedAt = value.HasValue ? DateTime.FromFileTimeUtc(value.Value) : (DateTime?)null;
        }

        public DateTime? IssuedAt { get; set; }

        /// <summary>
        /// Calendar year
        /// </summary>
        [Required]
        [Range(2018, 2030)]
        public short Year { get; set; }

        /// <summary>
        /// Calendar month
        /// </summary>
        [Range(1, 12)]
        [Required]
        public byte Month { get; set; }

        [Required]
        //1185SD2
        [StringLength(7)]
        [Display(Name = "Tax code")]
        public string TaxCode { get; set; }


        [Required]
        [StringLength(1)]
        [Display(Name = "NI Category")]
        public NICategory NICategory { get; set; }

        /// <summary>
        /// Gross payment & any post tax deduction per sample Payslip
        /// </summary>
        public virtual IList<QuantitativeAdjustment> Payments { get; set; } = new List<QuantitativeAdjustment>();

        /// <summary>
        /// Statuary calculations affecting pay
        /// </summary>
        public virtual IList<Adjustment> Adjustments { get; set; } = new List<Adjustment>();

        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
    }
}