﻿namespace µPayroll.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Company
    {
        public int Id { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "Company name")]
        public string Name { get; set; }

        public int AddressId { get; set; }

        public virtual Address Address { get; set; }

        public virtual IList<Employee> Employees { get; set; } = new List<Employee>();
    }
}