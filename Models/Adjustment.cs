﻿namespace µPayroll.Models
{
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    public class Adjustment : IAdjustment
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

	    [Required]
        public AdjustmentType AdjustmentType { get; set; }

        [Required]
        public decimal Value { get; set; }

        public override string ToString()
            => $"{Description} {Value:£0.00;£(0.00)}";

        public int PayslipAdjustmentId { get; set; }

        public virtual Payslip PayslipAdjustment { get; set; }
    }
}