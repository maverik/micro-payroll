﻿namespace µPayroll.Models
{
    using Data.Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    // Intentionally left like this rather than further Normalized state for practical reasons
    public class Address : IEquatable<Address>
    {
        public int Id { get; set; }

        [Required]
        // As per https://www.gov.uk/government/publications/bulk-data-transfer-for-sponsors-xml-schema
        [StringLength(35)]
        [Display(Name = "Address line 1")]
        public string Line1 { get; set; }

        [StringLength(35)]
        [Display(Name = "Address line 2")]
        public string Line2 { get; set; }

        [Required]
        // UK's longest city name happens to be 58 char long
        [StringLength(60)]
        public string City { get; set; }

        [Required]
        // As per https://www.gov.uk/government/publications/bulk-data-transfer-for-sponsors-xml-schema
        [StringLength(10)]
        //db stores this as ascii
        public string Postcode { get; set; }

        [Required]
        // Based of all the supported countries in Countries.Default.Values
        // Ideally I'd store ISO Country code only if this was going to be and internationally used
        // program and convert when needed in UI but leaving it as it is for sample. Infact I don't
        // even feel like storing this since it's going to be UK all the way through for this scenario
        // (and could easily be added in a later migration if needed) but just for completion sake..
        [StringLength(32)]
        public string Country { get; set; } = Countries.Default[Countries.IsoCountryCode.GB];

        public virtual IList<Company> Companies { get; set; } = new List<Company>();

        public virtual IList<Employee> Employees { get; set; } = new List<Employee>();

        public override string ToString() => string.Join("\n",
            new[] { Line1, Line2, City, Postcode, Country }.Where(x => !string.IsNullOrWhiteSpace(x)));

        #region IEquatable<Address> members
        public bool Equals(Address other) => !(other is null) && (ReferenceEquals(this, other) || string.Equals(Line1, other.Line1) &&
                                        string.Equals(Line2, other.Line2) && string.Equals(City, other.City) &&
                                        string.Equals(Postcode, other.Postcode) &&
                                        string.Equals(Country, other.Country));

        public override bool Equals(object obj) => !(obj is null) &&
                   (ReferenceEquals(this, obj) || obj.GetType() == GetType() && Equals((Address)obj));

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Line1 != null ? Line1.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Line2 != null ? Line2.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (City != null ? City.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Postcode != null ? Postcode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Country != null ? Country.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Address left, Address right) => Equals(left, right);

        public static bool operator !=(Address left, Address right) => !Equals(left, right);
        #endregion
    }
}