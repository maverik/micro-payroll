﻿namespace µPayroll.Models
{
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    //had to break inheritance from Adjustment as without TPC, this ends up in a mess
    public class QuantitativeAdjustment : IAdjustment
    {
        public int Id { get; set; }

        //I really want to use a float here but alas
        //Multiplication will get complex. I'm not sure
        //whats the purpose here for floating point but
        //since sample has a floating quantity..
        [Required]
        public decimal Quantity { get; set; }

        [Display(Name = "Unit cost")]
        [Required]
        public decimal UnitValue { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Display(Name = "Amount")]
        public decimal Value => Quantity * UnitValue;

        public int PayslipPaymentId { get; set; }

        public virtual Payslip PayslipPayment { get; set; }
    }
}