﻿namespace µPayroll.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;

    public class Employee
    {
        public int Id { get; set; }

        [Display(Name = "Employee ID")]
        /// <summary>
        /// Employee's Id as it appears on payroll system
        /// </summary>
        // randomly picked off the sample Payslip
        [StringLength(12)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// Employees full name
        /// </summary>
        [Required]
        //as per https://stackoverflow.com/a/30509/504757 + 35x2+2 spaces in the
        //event we have first/middle/last name in here
        [StringLength(72)]
        public string Name { get; set; }

        [Required]
        //as per https://www.datadictionary.nhs.uk/version2/data_dictionary/data_field_notes/n/national_insurance_number_de.asp
        [StringLength(9)]
        [Display(Name = "NI Number")]
        public string NICode { get; set; }

        // Normally I wouldn't put this field here as it can change over time
        // and I generally prefer to track this (especially for this scenario
        // where it can affect calculations if redone for some reason)
        // However for sample, I've opted to not track salary changes here
        [Required]
        [Display(Name = "Annual Salary")]
        [Range(typeof(decimal), "1", "79228162514264337593543950335", ErrorMessage =
            "The annual salary must be between 1 & 79 octillion.")]
        public decimal AnnualSalary { get; set; }


        [Display(Name = "Contract Reference")]
        //randomly picked as no sample value to go on
        [StringLength(60)]
        public string ContractReference { get; set; }

        public int AddressId { get; set; }

        public virtual Address Address { get; set; }
        
        public virtual IList<Payslip> Payslips { get; set; } = new List<Payslip>();

        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        public int? ManagerId { get; set; }

        public virtual Employee Manager { get; set; }

        public virtual IList<Employee> ManagedEmployees { get; set; }
    }
}